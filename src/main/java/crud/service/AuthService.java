package crud.service;

import crud.model.Auth;
import crud.repository.AuthRep;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthService {
    AuthRep authRep;

    public AuthService(AuthRep authRep) {
        this.authRep = authRep;
    }

    public List<Auth> getAllAuthorization(){
        return (List<Auth>) authRep.findAll();
    }

}
