package crud.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import crud.service.AuthService;

@Controller

public class AuthCont {
    private final AuthService authService;
    public AuthCont(AuthService authService) {
        this.authService = authService;
    }
    @GetMapping(path="/authorization")
    public ResponseEntity<?> getAllAuthorization(){
        return ResponseEntity.ok(authService.getAllAuthorization());
    }


}
