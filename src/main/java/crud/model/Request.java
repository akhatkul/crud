package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "request")
public class Request {
    @Id
    private long id;
    @Column
    private long requestUserId;
    private long responseUserId;
    private long caseId;
    private long caseIndexId;
    private String createdType;
    private String comment;
    private String status;
    private long timestamp;
    private long shareStart;
    private long shareSinish;
    private boolean favourite;
    private String declineNote;
    private long companyUnitId;
    private  long fromRequestId;
    private long updatedTimestamp;
    private long updatedBy;
}
