package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "notification")
public class Notification {
    @Id
    private long id;
    @Column
    private String objectType;
    private long objectId;
    private long companyUnitId;
    private long userId;
    private long createdTimestamp;
    private long viewedTimestamp;
    private boolean isViewed;
    private String title;
    private String text;
    private  long companyId;
}
