package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "auth")
public class Auth {
    @Id
    private long id;
    @Column
    private String username;
    private String email;
    private String password;
    private String role;
    private String forgotPasswordKey;
    private long ForgotPasswordKeyTimestamp;
    private long companyUnitId;

}
