package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class Users {
    @Id
    private long id;
    @Column
    private long authId;
    private String name;
    private String surname;
    private String secondName;
    private String status;
    private long companyUnitId;
    private String password;
    private long lastLoginTimestamp;
    private String iin;
    private boolean isActive;
    private boolean isActivated;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;
}
