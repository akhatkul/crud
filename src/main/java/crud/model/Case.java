package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "case_")
public class Case {
    @Id
    private long id;
    @Column
    private String caseNumber;
    private String caseTome;
    private String titleRu;
    private String titleKz;
    private String titleEn;
    private long startDate;
    private long finishedDate;
    private long pageNumber;
    private boolean edsSignatureFlag;
    private String edsSignature;
    private boolean sendingNafFlag;
    private boolean deletionFlag;
    private boolean limitedAccessFlag;
    private String hash;
    private int version;
    private String idVersion;
    private boolean activeVersionFlag;
    private String note;
    private long idLocation;
    private long idCaseIndex;
    private long idInventory;
    private long idDestructionAct;
    private long idStructuralSubdivision;
    private String blockchainAddress;
    private long blockchainAddDate;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;

}
