package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "company_unit")
public class CompanyUnit {
    @Id
    private long id;
    @Column
    private String nameRu;
    private String nameKz;
    private String nameEn;
    private long parentId;
    private long year;
    private long companyId;
    private String companyIndex;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;

}
