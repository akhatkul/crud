package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "search_key_routing")
public class SearchKeyRouting {
    @Id
    private long id;
    @Column
    private long searchKeyId;
    private String tableName;
    private long tableId;
    private String type;
}
