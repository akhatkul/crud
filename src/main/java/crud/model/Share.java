package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "share")
public class Share {
    @Id
    private long id;
    @Column
    private String note;
    private long senderId;
    private long receiverId;
    private long requestId;
    private long shareTimestamp;
}
