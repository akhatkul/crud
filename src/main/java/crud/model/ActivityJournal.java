package crud.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "activity_journal")
public class ActivityJournal {
    @Id
    private long id;
    @Column
    private String eventType;
    private String objectType;
    private long objectId;
    private long createdTimestamp;
    private long createdBy;
    private String messageLevel;
    private String message;
}
