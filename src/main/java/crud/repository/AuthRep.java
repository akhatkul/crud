package crud.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import crud.model.Auth;

@Repository

public interface AuthRep extends CrudRepository<Auth, Long> {

}
